import re
import subprocess
import requests
import xml.etree.ElementTree as ET
from utils import parse_color

def fetch_versions():
    process = subprocess.Popen(["git", "ls-remote", "--tags", "https://github.com/elementary/terminal.git"],
        stdout=subprocess.PIPE,
        universal_newlines=True)
    output = map(lambda line: re.findall("refs/tags/(\\d+\\.(\\d+.)*\\d+)$", line), process.stdout.readlines())
    output = map(lambda lst: lst[0][0], filter(lambda lst: len(lst) == 1, output))
    return list(dict.fromkeys(output)) # Removes duplicates

versions = fetch_versions()

_source = lambda version: \
        f"https://raw.githubusercontent.com/elementary/terminal/{version}/data/io.elementary.terminal.gschema.xml"

def download_colorscheme(version="master"):
    try:
        r = requests.get(_source(version))
    except requests.RequestException as rex:
        print(f"Could not fetch elementary colorscheme. Reason: {rex.strerror}")
        raise rex

    root = ET.fromstring(r.content)

    find_value = lambda name: \
            root.findall(f"*/key[@name='{name}']/default")[0].text.strip()[1:-1]

    colorscheme = {
        "name": "elementaryOS",
        "foreground": parse_color(find_value("foreground")),
        "background": parse_color(find_value("background")),
        "cursor_color": parse_color(find_value("cursor-color")),
        "ansi_colors": list(map(parse_color, find_value("palette").split(":")))
    }

    if version != "master":
        colorscheme["name"] += "-" + version

    return colorscheme
