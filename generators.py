import os
import plistlib
import json

from utils import rgb_to_hex, rgb_to_double_hex, rgb_rescale
from pathlib import Path

_generators = []

def generator(name):
    def decorator(func):
        def wrapper(*args, **kwargs):
            print(f"\033[92mGenerating colorscheme for {name}\033[0m")
            try:
                return func(*args)
            except Exception as ex:
                print(f"\033[91mError: Failed to generate colorscheme.\nReason: \033[0m{str(ex)}")
        _generators.append(wrapper)
        return wrapper
    return decorator

def generate_all(colorscheme, prefix = ""):
    Path(prefix).mkdir(parents=True, exist_ok=True)
    for gen in _generators: gen(colorscheme, prefix)

@generator("iTerm2")
def iterm2_generate(colorscheme, prefix = ""):
    out = {}
    clr_to_dict = lambda clr: (lambda clr_rgb: { \
        "Color Space": "sRGB",
        "Red Component": clr_rgb[0],
        "Green Component": clr_rgb[1],
        "Blue Component": clr_rgb[2],
        "Alpha Component": clr_rgb[3]})(rgb_rescale(*clr))
    for i in range(0, len(colorscheme["ansi_colors"])):
        out[f"Ansi {i} Color"] = clr_to_dict(colorscheme["ansi_colors"][i])
    out["Background Color"] = clr_to_dict(colorscheme["background"])
    out["Foreground Color"] = clr_to_dict(colorscheme["foreground"])
    out["Badge Color"] = clr_to_dict((255, 38, 0, 127))
    out["Bold Color"] = clr_to_dict(colorscheme["foreground"])
    out["Cursor Color"] = clr_to_dict(colorscheme["cursor_color"])
    out["Cursor Text Color"] = clr_to_dict(colorscheme["background"])
    out["Cursor Guide Color"] = clr_to_dict((179, 236, 255, 63))
    out["Selection Color"] = clr_to_dict(colorscheme["foreground"])
    out["Selected Text Color"] = clr_to_dict(colorscheme["background"])
    out["Link Color"] = clr_to_dict((0, 91, 187, 255))
    file = open(os.path.join(prefix, f"{colorscheme['name']}.itermcolors"), "wb")
    plistlib.dump(out, file)
    file.close()

@generator("XFCE4-Terminal")
def xfce4_terminal_generate(colorscheme, prefix = ""):
    conv_rgb = lambda clr: rgb_to_double_hex(clr[0], clr[1], clr[2])
    rgb = lambda name: conv_rgb(colorscheme[name])
    file = open(os.path.join(prefix, f"{colorscheme['name']}.theme"), "w")
    file.write("[Scheme]\n")
    file.write(f"Name={colorscheme['name']}\n")
    file.write(f"ColorCursor={rgb('cursor_color')}\n")
    file.write(f"ColorForeground={rgb('foreground')}\n")
    file.write(f"ColorBackground={rgb('background')}\n")
    palette = map(conv_rgb, colorscheme["ansi_colors"])
    file.write(f"ColorPalette={';'.join(palette)}")
    file.close()

@generator("Windows-Terminal")
def windows_terminal_generate(colorscheme, prefix = ""):
    conv_rgb = lambda clr: rgb_to_hex(clr[0], clr[1], clr[2])
    out = {}
    out["name"] = colorscheme["name"]
    out["background"] = conv_rgb(colorscheme["background"])
    out["foreground"] = conv_rgb(colorscheme["foreground"])
    out["cursorColor"] = conv_rgb(colorscheme["cursor_color"])
    out["selectionBackground"] = conv_rgb(colorscheme["foreground"])
    color_names = [
        "black", "red", "green", "yellow", "blue", "purple", "cyan", "white",
        "brightBlack", "brightRed", "brightGreen", "brightYellow", "brightBlue",
        "brightPurple", "brightCyan", "brightWhite" ]
    out.update(zip(color_names, map(conv_rgb, colorscheme["ansi_colors"])))
    file = open(os.path.join(prefix, f"{colorscheme['name']}-Windows-Terminal.json"), "w")
    json.dump(out, file, indent = 2)
    file.close()
