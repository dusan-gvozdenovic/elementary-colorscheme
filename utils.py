def parse_color(str):
    if str[0] == "#":
        return parse_hex(str[1:])
    elif str.find("rgb(") or str.find("rgba("):
        t = list(map(lambda x: x.strip(), str.strip("rgba()").split(",")))
        return int(t[0]), int(t[1]), int(t[2]), float(t[3])
    else:
        raise Exception("The supplied string was not recognized as a color")

def rgb_to_hex(r, g, b):
    return f"#{r:02x}{g:02x}{b:02x}"

def rgb_to_double_hex(r, g, b):
    return "#{0:02x}{0:02x}{1:02x}{1:02x}{2:02x}{2:02x}".format(r, g, b)

def rgba_to_double_hex(r, g, b, a = 255):
    return "#{0:02x}{0:02x}{1:02x}{1:02x}{2:02x}{2:02x}{3:02x}{3:02x}".format(r, g, b, a)

# def rgb_to_srgb(r, g, b, a = 255):
#     correction = lambda v: \
#         12.92 * v if v <= 0.0031308 else \
#         1.055 * v ** (1 / 2.4) - 0.055
#     return correction(r / 255), correction(g / 255), correction(b / 255), correction(a / 255)

def parse_hex(str):
    length = len(str)
    if length not in [3, 6, 9, 12]:
        raise Exception("The supplied string was not recognized as a color")
    length //= 3
    vals = [str[:length], str[length:2 * length], str[2 * length:]]
    r, g, b = map(lambda x: int(f"0x{x}", 0), vals)
    return r, g, b

def rgb_rescale(r, g, b, a = 255):
    return r / 255, g / 255, b / 255, a / 255

# def linear_to_sRGB(c):
#     return c / 12.92 if c <= 0.04045 else ((c + 0.055) / 1.055) ** 2.4
