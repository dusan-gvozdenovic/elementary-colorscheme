# elementaryOS Colorscheme Generator
Simple Python script that parses elementary colorscheme and generates appropriate format for your favorite terminal emulator.

![Screenshot](https://gitlab.com/dusan-gvozdenovic/elementaryos-colorscheme-generator/-/raw/master/screenshot.png?inline=true)

# Supported terminals
- iTerm2
- XFCE4 Terminal
- Windows Terminal
- Not in the list? Submit an issue or a pull request!

## Requirements
```bash
pip install -r requirements.txt
```

## Usage
```bash
./elementary-gen.py (list-versions) | (gen <version>|<all>)
```

*Note:* By default, generated colorschemes will be placed in ```output/``` subdirectory.
