#!/usr/bin/env python3

import sys
import generators as gens
from download import download_colorscheme, versions

output_prefix = "output"

def exit_help():
    sys.exit(f"Usage: {sys.argv[0]} (list-versions) | (gen <version>|<all>)")

if len(sys.argv) < 2 or len(sys.argv) > 3:
    exit_help()

cmd = sys.argv[1]

if cmd == "list-versions":
    print("Versions: ", versions)
    sys.exit(0)

if cmd != "gen" or len(sys.argv) != 3:
    exit_help()

def gen_version(version):
    print(f"Generating for version: {version}")
    try:
        colorscheme = download_colorscheme(version)
    except:
        print("Failed to generate colorscheme for version " + version)
        return
    print("Foreground:", colorscheme["foreground"])
    print("Background:", colorscheme["background"])
    print("Cursor color:", colorscheme["cursor_color"])
    print("ANSI colors:", colorscheme["ansi_colors"])
    gens.generate_all(colorscheme, output_prefix)

version = sys.argv[2]

if version == "all":
    for version in versions:
        gen_version(version)
    sys.exit(0)

if version not in versions:
    sys.exit(f"Version {version} does not exist!")
gen_version(version)

